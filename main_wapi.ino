#include <SoftwareSerial.h> //Biblioteca para comunicação serial com o Módulo SIM800C
#include <OneWire.h>  //Biblioteca para leitura dos sensores DS18B20 
#include <DallasTemperature.h> //Biblioteca para manipulação dos sensores conectados

#define pin_temp 3 //Definição da porta dos sensores de temperatura
#define ph_pin A0 //Definição da porta do sensor de pH
#define turb_pin A1 //Definição da porta do sensor de turbidez


DeviceAddress tempSup = { 0x28, 0xE7, 0xA1, 0x77, 0x91, 0x11, 0x2, 0x75 };
DeviceAddress tempCol = { 0x28, 0x4B, 0x9A, 0x77, 0x91, 0x19, 0x2, 0xD6 };  // Endereço dos sensores DS18B20 conectados
DeviceAddress tempSub = { 0x28, 0x3A, 0x68, 0x77, 0x91, 0x10, 0x2, 0xEE };

SoftwareSerial GPRS(7, 8);  
OneWire oneWire(pin_temp);  
DallasTemperature tempSensors(&oneWire);

struct dataCollected{
  String timeStamp;
  float tempSup=0.0;float tempCol=0.0;float tempSub=0.0;
  float ph=0.0; float turb=0.0; float od=0.0;
  float cond=0.0; float sal=0.0; float amon=0.0;};

String gsmWrite(String snd, String rcv);

void power(){
  pinMode(9, OUTPUT);
  digitalWrite(9, 0);
  delay(2000);
  Serial.println("POWER ON/OFF");
  digitalWrite(9, 1);
  delay(7000);
  }
    
void setup() {
  Serial.begin(9600);
  power();
  GPRS.begin(19200);
  
  Serial.println("Checando...");
  String x=gsmWrite("AT", "OK");
  Serial.println(x);
  if (x == "FAIL"){    
    Serial.println("Reiniciando...");
    power();
    setup();}
  else{initConnection();}
}
 
void loop(){
  dataCollected nowData=requestCollectData();
  delay(500);
  Serial.print("Time Stamp: "+nowData.timeStamp+"\n");
  Serial.print("Turbidez:"+String(nowData.turb)+"\n");
  Serial.print("Temperatura Superfície:"+String(nowData.tempSup)+"\n");
  Serial.print("Temperatura Coluna:"+String(nowData.tempCol)+"\n");
  Serial.print("Temperatura Substrato:"+String(nowData.tempSub)+"\n");
  Serial.print("pH: "+String(nowData.ph)+"\n");
  Serial.print("Oxigênio Dissolvido: "+String(nowData.od)+"\n");
  Serial.print("Condutividade: "+String(nowData.cond)+"\n");
  Serial.print("Salinidade: "+String(nowData.sal)+"\n");
  Serial.print("Amônia: "+String(nowData.amon)+"\n");
  sendData(nowData);
  //delay(1200000); 
  
  }

float checkTemp(float temp){
  if (temp!=-127.0){return temp;}
  else{return 0.0;}} 

dataCollected requestCollectData(){
  dataCollected nowData;
 
  nowData.timeStamp = gsmTIME(); //Time Stamp
  
  tempSensors.requestTemperatures(); // Temperatures
  nowData.tempSup = checkTemp(tempSensors.getTempC(tempSup));
  nowData.tempCol = checkTemp(tempSensors.getTempC(tempCol));
  nowData.tempSub = checkTemp(tempSensors.getTempC(tempSub));

  int measurePh = analogRead(ph_pin); //pH
  float voltagePh = (5/1024.0)*measurePh; 
  nowData.ph = 7+((2.5 - voltagePh)/0.18);

  int turbValue = analogRead(turb_pin); //Turbidity
  float voltageTurb = turbValue * (5.0/1024.0);
  nowData.turb = -(1120.4*voltageTurb*voltageTurb)+(5742.3*voltageTurb)-4352.9;

  return nowData;}

void initConnection(){
  int check=1;
  if(gsmWrite("AT+SAPBR=2,1",",3,")=="FAIL"){
    gsmWrite("AT+SAPBR=0,1","OK");}
  gsmWrite("AT+HTTPTERM","OK");
  if(gsmWrite("AT+SAPBR=3,1,\"Contype\",\"GPRS\"","OK")=="FAIL"){check=0;}
  if(gsmWrite("AT+SAPBR=3,1,\"APN\",\"claro.com.br\"","OK")=="FAIL"){check=0;}
  if(gsmWrite("AT+SAPBR=1,1","OK")=="FAIL"){check=0;}
  if(gsmWrite("AT+HTTPINIT","OK")=="FAIL"){check=0;}
  if(gsmWrite("AT+HTTPSSL=1","OK")=="FAIL"){check=0;}
  if(gsmWrite("AT+HTTPPARA=\"CID\",1","OK")=="FAIL"){check=0;}
  if(gsmWrite("AT+HTTPPARA=\"URL\",\"https://wapi-4d092.firebaseio.com/data_collected/.json\"","AT+HTTPPARA")=="FAIL"){check=0;}
  if(check==0){initConnection();}}

void sendData(dataCollected data){
  String data_to_send= "{\"time_stamp\":"+ data.timeStamp +",\"temp\":{\"sup\":"+String(data.tempSup)+",\"col\":"+String(data.tempCol)+",\"sub\":"+String(data.tempSub)+"},\"turb\":"+String(data.turb)+",\"ph\":"+String(data.ph)+",\"od\":"+String(data.od)+",\"cond\":"+String(data.cond)+",\"sal\":"+String(data.sal)+",\"ammonia\":"+String(data.amon)+"}";
  GPRS.println("AT+HTTPDATA=500,1000");
  delay(200);
  GPRS.println(data_to_send);
  delay(1000);
  if(gsmWrite("AT+HTTPACTION=1",",200,")=="FAIL"){Serial.println("Falhou..");initConnection();}}

String gsmWrite(String snd, String rcv){
  GPRS.println(snd);
  for (uint16_t i = 0; i < 1800; i++){
    delay(25);
    if (GPRS.available()){
      delay(50);
      String a = GPRS.readString(); 
      Serial.println(a);
      if(a.indexOf(rcv) > -1){return a;}
      else if(a.indexOf("ERROR")>-1 || a.indexOf(",603,")>-1 || a.indexOf(",601,")>-1 || a.indexOf(",605,")>-1 || a.indexOf("+SAPBR: 1,1,")>-1){return "FAIL";}}}
  return "FAIL";}
 
String gsmTIME(){
  String c = gsmWrite("AT+CCLK?", "+CCLK:");
  int16_t a = c.indexOf("\"") + 1;
  int16_t b = c.indexOf("-", a);
  c = c.substring(a, b);
  c.replace(",","");c.replace(":","");c.replace("/",""); c="20"+c;
  return c;}

/*

*/ 
